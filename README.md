## Description
Bootstrap scripts for various Operating Systems.

- Windows 10
- Debian
- Fedora

## Install
### Windows
https://gitlab.com/jsmarble/os-bootstrap/-/blob/master/win10/README.md

### Debian
https://gitlab.com/jsmarble/os-bootstrap/-/blob/master/linux/debian/README.md
