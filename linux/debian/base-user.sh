rm $HOME/ssh-keys.sh
wget https://gitlab.com/snippets/1909380/raw -O ssh-keys.sh
sh ssh-keys.sh
ln -s $(pwd)/ssh-keys.sh $HOME/ssh-keys.sh

curl -L git.io/antigen > $HOME/antigen.zsh
sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"

git config --global user.email $GIT_USER_EMAIL
git config --global user.name $GIT_USER_NAME
