#!/bin/bash

# Prompt the user for input
read -p "Enter the machine name: " machine_name

# Reset hostname
OLDHOST=$(hostname)
hostnamectl set-hostname $machine_name

# Regen machine-id
rm /etc/machine-id
rm /var/lib/dbus/machine-id
systemd-machine-id-setup
ln -s /etc/machine-id /var/lib/dbus/machine-id

rm -f /etc/resolv.conf
ln -s /run/systemd/resolve/resolv.conf /etc/resolv.conf

sed -i -e "s/ubuntu-template/$machine_name/g" /etc/hosts
sed -i -e "s/$OLDHOST/$machine_name/g" /etc/hosts

rm /var/log/kern* &>/dev/null
rm /var/log/messages* &>/dev/null

read -p "Press [Enter] to reboot ..."
reboot
