apt-get update
apt-get -y install sudo curl wget unzip htop apt-transport-https
apt-get -y install software-properties-common gnupg nfs-common zsh ack silversearcher-ag
/usr/sbin/usermod -aG sudo joshua
