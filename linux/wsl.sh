WIN_PATH="$(realpath /mnt/c/Users/[Jj]*)" # Change to your username or first letter to match pattern
ln -s $WIN_PATH $HOME/win
cp -r $HOME/win/.ssh $HOME
chmod --quiet 700 $HOME/.ssh
chmod --quiet 644 $HOME/.ssh/authorized_keys
chmod --quiet 644 $HOME/.ssh/known_hosts
chmod --quiet 644 $HOME/.ssh/config
chmod --quiet 600 $HOME/.ssh/id_rsa
chmod --quiet 644 $HOME/.ssh/id_rsa.pub
