# wget https://gitlab.com/snippets/1909380/raw -O ssh-keys.sh

mkdir -p ~/.ssh
wget -O ~/.ssh/authorized_keys https://gitlab.com/snippets/1909376/raw

chmod --quiet 700 ~/.ssh
chmod --quiet 644 ~/.ssh/authorized_keys
chmod --quiet 644 ~/.ssh/known_hosts
chmod --quiet 644 ~/.ssh/config
chmod --quiet 600 ~/.ssh/id_rsa
chmod --quiet 644 ~/.ssh/id_rsa.pub
