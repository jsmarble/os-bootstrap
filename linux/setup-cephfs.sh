user=$1
host=$2

apt -y install ceph-common

mkdir -p -m 755 /etc/ceph
ssh root@${host} "sudo ceph config generate-minimal-conf" | sudo tee /etc/ceph/ceph.conf

chmod 644 /etc/ceph/ceph.conf

ssh root@${host} "sudo ceph fs authorize cephfs client.${user} / rw" | sudo tee /etc/ceph/ceph.client.${user}.keyring

chmod 600 /etc/ceph/ceph.client.${user}.keyring

# Use sed to extract the key value
key_value=$(sed -n -r -E 's/^\s*key[[:space:]]*=[[:space:]]*([^[:space:]]+).*$/\1/p' /etc/ceph/ceph.client.${user}.keyring)
key_file=/etc/ceph/ceph.client.${user}.key

# Check if key value is found
if [[ -n "$key_value" ]]; then
    # Save the key value to a file
    echo "$key_value" > $key_file
    echo "Key value extracted and saved to file $key_file"
    chmod 600 $key_file
else
    echo "Key value not found in the configuration file."
fi

echo "Writing to /etc/fstab ..."
echo "10.0.1.5:6789:/ /mnt/cephfs ceph name=$user,secretfile=$key_file,noatime,_netdev 0 0" >> /etc/fstab


