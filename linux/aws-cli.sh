wget -q -O $HOME/awscliv2.zip "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip"
unzip -o $HOME/awscliv2.zip -d $HOME
sudo $HOME/aws/install --update
rm $HOME/awscliv2.zip
