# Install scoop
Invoke-WebRequest -useb get.scoop.sh | Invoke-Expression
scoop install git

scoop bucket add extras
scoop bucket add nerd-fonts
scoop bucket add dodorz https://github.com/dodorz/scoop
scoop update

git clone https://gitlab.com/jsmarble/os-bootstrap.git
cd os-bootstrap\win10
